#!/usr/bin/env python
import tensorflow as tf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# Model parameters
W = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
# Model input and output
x = tf.placeholder(tf.float32)
linear_model = W * x + b
y = tf.placeholder(tf.float32)

# loss
loss = tf.reduce_sum(tf.square(linear_model - y))  # sum of the squares

# optimizer
# optimizer = tf.train.GradientDescentOptimizer(0.001)
# optimizer = tf.train.AdamOptimizer(0.05)
optimizer = tf.train.FtrlOptimizer(0.3)

train = optimizer.minimize(loss)

# training data
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]
# training loop
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)  # reset values to wrong

losses = []
curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
print("W: %s b: %s loss: %s" % (curr_W, curr_b, curr_loss))
losses.append(curr_loss)

for i in range(1000):
    sess.run(train, {x: x_train, y: y_train})
    curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
    losses.append(curr_loss)

    print("W: %s b: %s loss: %s" % (curr_W, curr_b, curr_loss))

plt.plot(range(1, 1002), losses)
plt.ylabel('Loss function value')
plt.yscale('log')
plt.xlabel('Iteration number')
plt.savefig('plot.png')
